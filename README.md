# DOODL SilverStripe Project Installer #
## Overview ##
Installer for creating a new SilverStripe project. Forked from `silverstripe/installer`, however it adds a reasonable amount of default configuration via this package and via the `doodlltd/silverstripe-4-boilerplate` package.

## Creating a new SilverStripe project ##

New projects should be created by way of the `doodlltd/silverstripe-project` installer.

To do so, run the following command:
```
composer create-project doodlltd/silverstripe-project . dev-master --repository="{\"url\": \"git@bitbucket.org:doodlltd/silverstripe-project.git\", \"type\":\"vcs\"}" --remove-vcs
```
>If you find that composer is unable to resolve a working set of packages for your development environment, use the `--ignore-platform-reqs` option flag.

>To disable the installation scripts which have been configured, use the `--noscripts` option flag.

### After Installation ###
Composer should automatically install the appropriate **npm** packages for you. If not, run:
```
npm install
```
#### Post Installation Checklist ####
1. Initialise a new **git** repository for this project with `git init`
2. If you plan on connecting to a remote repository, do so now using `git remote add orign {repository-url}`. *Doing so will add the remote repository automatically in the next step*
3. Update the `package.json` file with new project details by running `npm init` and follow the prompts.
4. Update the following project specifics:
   1. Dev server username & host in `package.json` **devsync** script
   2. Update git repository in `deploy.php`
   3. Update dev server host and username in `deploy.php`
   4. Optionally update project name and description in `composer.json`
5. Commit the initial configuration to **git** `git add -A && git commit -m "Initial Config"`
6. Generate the project CHANGELOG by running `npm run release -- --first-release`