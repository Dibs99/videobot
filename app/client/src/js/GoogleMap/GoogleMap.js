


export default function GoogleMap() {

    try {
        const element = document.querySelectorAll('[data-map-data]');
        const data = JSON.parse(element[0].dataset.mapData);
    
    // if it's a dynamic map
    if (data.mode === 'interactive') {
   
        let map;
        let service;
        let infowindow;

        window.initMap = () => {
        const location = new google.maps.LatLng(data.latLng);
        infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(element[0], {
            center: location,
            zoom: data.zoom,
        });
        const request = data.request;
        service = new google.maps.places.PlacesService(map);
        service.findPlaceFromQuery(request, (results, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
            for (let i = 0; i < results.length; i++) {
                createMarker(results[i]);
            }
            map.setCenter(results[0].geometry.location);
            }
        });
        }

        const createMarker = (place) => {
        const marker = new google.maps.Marker({
            map,
            position: place.geometry.location,
        });
        google.maps.event.addListener(marker, "click", () => {
            infowindow.setContent(place.name);
            infowindow.open(map);
        });
        }
    }
    }
    catch(err) {
        console.log(err);
    }
    
} 

