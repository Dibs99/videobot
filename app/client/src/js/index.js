import "../scss/style.scss";

import { MobileMenuDrawer, Hamburger } from "@doodl/slate";
import { AnimationScroll } from "./Animations/Animations"
import Accordion from './Accordion';
// import GoogleMapDynamic from './GoogleMap/GoogleMap';

// const menus = StandardMenuConfiguration();
const menuDrawer = new MobileMenuDrawer();
new Hamburger({
    onOpen: function onOpen() {
      menuDrawer.open();
    },
    onClose: function onClose() {
      menuDrawer.close();
    }
  });
Accordion();
new AnimationScroll();

// GoogleMapDynamic();
// Webfonts
import WebFont from 'webfontloader';

// FontAwesome Icons
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faFacebookSquare, faLinkedin, faYoutube, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faPaperPlane, faEnvelopeSquare, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';

// If an instance of react-contact-form exists on page, render it
const formRoot = document.getElementById("react-contact-form");
if (formRoot) {
    import(
        /* webpackChunkName: "DDLReactForm" */ "./ReactContactForm/ReactContactForm"
    )
        .then(({ default: Form }) => {
            Form(formRoot);
        })
        .catch((error) => console.error(error));
}

WebFont.load({
    google: {
        families: ['Montserrat:300,900&display=swap']
    }
});

// // Enable Icons in FA Library
library.add(
    faFacebookSquare,
    faLinkedin,
    faYoutube,
    faInstagram,
    faPaperPlane,
    faEnvelopeSquare,
    faPhoneAlt
);

// Watch the DOM for elements which should be replaced by icons
dom.watch();
