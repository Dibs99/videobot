import "react-app-polyfill/stable";
/**
 * Renders a React Form from SilverStripe FormSchema
 */
import React from "react";
import ReactDOM from "react-dom";

import { FormLoader } from "@doodl/ss-react-forms";

const Form = ({ schemaID }) => {
    return <FormLoader schemaUrl={schemaID} />;
};

export default (element) => {
    const schemaID = element.getAttribute("data-schema-id");
    ReactDOM.render(<Form schemaID={schemaID} />, element);
};
