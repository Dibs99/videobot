import { gsap } from "gsap";



export default function Accordion() {

    gsap.utils.toArray(".tab").forEach(tab => {
        let toggle = false;
        let content = tab.querySelector(".tab-content"),
            line = tab.querySelector(".tab-label-button-line"),
            tl = gsap.timeline({ paused: true });
        const contentHeight = content.children[0].clientHeight + 50;
        tl.to(content, {height: contentHeight, duration: .5})
           .to(line, {rotation: 90}, "-=.3");

        tab.addEventListener("click", () => {
            if (toggle === false) {
                tl.play()
                toggle = !toggle;
            } else {
                tl.reverse()
                toggle = !toggle;
            }
        });
    })
}

