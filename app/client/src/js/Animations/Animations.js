import { gsap } from 'gsap'
import { ScrollTrigger } from "gsap/ScrollTrigger";


export class AnimationScroll {

    options = {
        fadeInUp: ".fade-in-animation",
        gradientFade: "body",
        textRollIn: ".text-fade-in",
        boxCenterToPosition: [".block", ".block-type-2"],
        biggerAndSmaller: ".block",
        imgSetIn: ".img-set-in",
        gradientSplash: "body",
        colours: {
            colourStop1: '#eef4f5',
            colourStop2: '#80ced6',
            colourStop3: '#6bcbbe',
            colourStopDark: '#4d968d',
            colourStopEnd: '#45948a',

        }
    }

    #timeline = null;

    constructor(options) {
        
        this.options = {
            ...this.options,
            ...options
        }
        this.#init()
    }

    #init = () => {
        gsap.registerPlugin(ScrollTrigger);
        this.fadeInUp();
        this.textRollIn();
        this.boxCenterToPosition();
        // this.biggerAndSmaller('box-shadow', '5px 10px 40px 0px #424141');
        this.imgSetIn();
        this.gradientSplash();
    }

    fadeInUp = () => {
        if (document.querySelector(this.options.fadeInUp)){
            gsap.from(this.options.fadeInUp, {
                delay: 1,
                y: 100,
                opacity: 0,
                duration: .5
            });
            
        }
    }

    gradientSplash = () => {
        const colours = this.options.colours;
        if (document.querySelector(this.options.gradientSplash)){
            
            this.timeline.fromTo(this.options.gradientSplash, {
                backgroundImage: "linear-gradient(45deg, black, transparent)"
            },{
                delay: .5,
                duration: 2,
                backgroundImage: `radial-gradient(${colours.colourStop1}, ${colours.colourStop3})`
            })
              .to(this.options.gradientSplash, {
                backgroundImage: `linear-gradient(133deg, ${colours.colourStop1}, ${colours.colourStop2}, ${colours.colourStop3})`,
            }, "-=.3").then(() => {
                this.linearGradShimmer();
            })
            this.timeline.play();
            
        }
    }

    linearGradShimmer = () => {
        const colours = this.options.colours;

        if (document.querySelector(this.options.gradientFade)) {
            gsap.to(this.options.gradientFade,
                {
                scrollTrigger: {
                    trigger: this.options.gradientFade,
                    once: true,
                    start: 'top top',
                    bottom: 'bottom bottom',
                    scrub: true,
                },
                backgroundImage: `linear-gradient(300deg, ${colours.colourStopEnd}, ${colours.colourStop3}, ${colours.colourStop2})`,
                })
            }
            
    }

    textRollIn = () => {
        if (document.querySelector(this.options.textRollIn)) {
            const elementArray = gsap.utils.toArray(this.options.textRollIn);
            elementArray.forEach((el, i) => {

            const wrapper = document.createElement('div');

            wrapper.setAttribute("style", "overflow: hidden;");
            el.parentNode.insertBefore(wrapper, el);
            wrapper.appendChild(el);

            gsap.from(el, {
                scrollTrigger: {
                    trigger: el,
                    once: true,
                    start: 'top 75%',
                },
                y: "100%",
                skewX: 10,
                duration: .2
            });
        })
     }
    }

    boxCenterToPosition = () => {
        if (document.querySelector(this.options.boxCenterToPosition)) {
        gsap.utils.toArray(this.options.boxCenterToPosition).forEach((el, i) => {
            const leftOrRight = i % 2 === 0 ? '57%' : '-57%';
            gsap.from(el, {
                scrollTrigger: {
                    trigger: el,
                    toggleActions: "play none none play",
                    start: 'top 75%',
                },
                x: leftOrRight,
                duration: 1,
            })
        })
    }
    }

    biggerAndSmaller = (property, value) => {
        if(document.querySelector(this.options.biggerAndSmaller)) {
            gsap.utils.toArray(this.options.biggerAndSmaller).forEach(el => {
                gsap.to(el, {
                scrollTrigger: {
                    trigger: el,
                    toggleActions: "play none none none",
                },
                duration: 1.5,
                [property]: value,
                repeat: -1,
                yoyo: true,
              });
            })
        }
    }

    imgSetIn = () => {
        if(document.querySelector(this.options.imgSetIn)) {
            gsap.utils.toArray(this.options.imgSetIn).forEach(img => {
                gsap.from(img, {
                    scrollTrigger: {
                        trigger: img,
                        start: 'top center',
                        once: true
                    },
                    duration: 1,
                    y: -50,
                })
            })
        }
    }

    get timeline() {
        if (!this.#timeline) {
            this.#timeline = gsap.timeline({ paused: true });
          }
          return this.#timeline;
    }
    
}
