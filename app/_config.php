<?php

/**
 * Make sure we do the basic config handled by the recipe.
 * Ensures we don't miss anything as part of recipe-core updates
 */
require __DIR__ . '../../vendor/silverstripe/recipe-core/app/_config.php';

use SilverStripe\Control\Director;
use SilverStripe\View\Parsers\ShortcodeParser;
use SilverStripe\Forms\HTMLEditor\HtmlEditorConfig;
use SilverStripe\Forms\HTMLEditor\TinyMCEConfig;
use DDL\Utility\GoogleMap;

/**
 * By default, force SSL in live environments
 */
if (Director::isLive()) {
    Director::forceSSL();
}

ShortcodeParser::get('default')->register('googlemap', function($arguments, $address, $parser, $shortcode) {
   $gmap = GoogleMap::create();

   $gmap->address = (isset($address, $address)) ? $address : $gmap->address;
   $gmap->zoom = (isset($arguments['zoom'], $arguments['zoom'])) ? $arguments['zoom'] : $gmap->zoom;
   $gmap->mode = (isset($arguments['mode'], $arguments['mode'])) ? $arguments['mode'] : $gmap->mode;
   $gmap->longitude = (isset($arguments['longitude'], $arguments['longitude'])) ? $arguments['longitude'] : $gmap->longitude;
   $gmap->latitude = (isset($arguments['latitude'], $arguments['latitude'])) ? $arguments['latitude'] : $gmap->latitude;
   $gmap->width = (isset($arguments['width'], $arguments['width'])) ? $arguments['width'] : $gmap->width;
   $gmap->height = (isset($arguments['height'], $arguments['height'])) ? $arguments['height'] : $gmap->height;
   $gmap->desktopWidth = (isset($arguments['desktopWidth'], $arguments['desktopWidth'])) ? $arguments['desktopWidth'] : $gmap->desktopWidth;
   $gmap->desktopHeight = (isset($arguments['desktopHeight'], $arguments['desktopHeight'])) ? $arguments['desktopHeight'] : $gmap->desktopHeight;
   return $gmap->forTemplate();      
});

$formats = [
    [
        'title' => 'Buttons', 'items' => [
            [
                'title' => 'Button',
                'classes' => 'button primary contained',
                'selector' => 'a',
                'forced_root_blocks' => false,
            ],
            [
                'title' => 'ButtonContainer',
                'classes' => 'button-grid',
                'block' => 'div',
                'wrapper' => true,
                'merge_siblings' => false,
                'forced_root_blocks' => false,
            ]
        ]
    ],
];

$image_size_presets = [
    [
        'width' => 100,
        'text' => 'Small fit',
        'name' => 'smallfit',
        'default' => true
    ],
    [
        'width' => 600,
        'i18n' =>  TinyMCEConfig::class . '.BEST_FIT',
        'text' => 'Medium fit',
        'name' => 'mediumfit'
    ],
    [
        'i18n' =>  TinyMCEConfig::class . '.ORIGINAL_SIZE',
        'text' => 'Original size',
        'name' => 'originalsize'
    ]
];
HtmlEditorConfig::get('cms')
        ->addButtonsToLine(1, 'styleselect')
        ->addButtonsToLine(2, 'blockquote')
        ->setOptions([
            'importcss_append' => true,
            'style_formats' => $formats,
            'extended_valid_elements' => 'i[class]',
            'image_size_presets' => $image_size_presets
        ]);