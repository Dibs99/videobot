<section class="$ClassName.Shortname.LowerCase<% if $StyleVariant %> $StyleVariant<% end_if %><% if $ExtraClass %> $ExtraClass<% end_if %>" id="section--$Anchor">
    $Element
</section>