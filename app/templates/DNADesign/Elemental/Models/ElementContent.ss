<div class="typography">
    <% if $ShowTitle %>
        <h1 class="big-title">$Title</h1>
    <% end_if %>
    $HTML
</div>
