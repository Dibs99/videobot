<main>
    <section>
        <div class="typography text-center">
            <h1 class="header--title">$Title</h1>
        </div>
        <div class="container center vertical-padding">
            <dl class="tabs typography">
                <% loop $Faqs %>
                    <div class="tab">
                        <div class="tab-label">
                            <dt class="question-title">$Question</dt>
                            <div class="tab-label-button">
                                <div class="tab-label-button-line"></div>
                            </div>
                        </div>
                        <dd class="tab-content">
                            $Answer
                        </dd>
                    </div>
                <% end_loop %>
            </dl>
        </div>
        <div class="button-grid">
            $Content
        </div>
    </section>
</main>