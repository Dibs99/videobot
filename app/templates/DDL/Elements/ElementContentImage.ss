<div class="container center text-center">
    <% if $ContentImage %>
        <img src="$ContentImage.ScaleMaxWidth(1000).URL" alt="$ContentImage.Title" class="element-image element-image--$AspectRatio img-set-in"/>
    <% end_if %>
    <div class="typography">
        <% if $ShowTitle %>
            <% if $SectionedPage && $First %>
                <h1>$Title</h1>
            <% else %>
                <h2 class="text-fade-in">$Title</h2>
            <% end_if %>
        <% end_if %>
        <% if $Subtitle %>
            <h3>$Title</h3>
        <% end_if %>
        $HTML
    </div>
</div>