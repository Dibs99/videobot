<div class="container center text-center">
    <div class="typography">
        <% if $ShowTitle %>
            <h2 class="text-fade-in">$Title</h2>
        <% end_if %>
        $HTML
    </div>
    $PaddedGrid
</div>

