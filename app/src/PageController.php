<?php

namespace {

    use DDL\Traits\PageControllerSchemaTrait;

    use SilverStripe\Control\Director;
    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;

    class PageController extends ContentController
    {
        use PageControllerSchemaTrait;

        private static $allowed_actions = [];

        protected function init()
        {
            parent::init();
            // Use webpack dev server build in test environments
            if (Director::isDev()) {
                if ($this->URLSegment != 'Security') {
                    // Only require JS because dev mode will load the CSS inline to make use of HMR
                    Requirements::javascript(
                        '//localhost:8001/dev/js/main.bundle.js'
                    );
                } else {
                    // Require 'security' as JS so we get hot reloading
                    Requirements::javascript(
                        '//localhost:8001/dev/js/security.bundle.js'
                    );
                }
            } else {
                if ($this->URLSegment != 'Security') {
                    Requirements::css('app/client/dist/css/main.bundle.css');
                    Requirements::javascript(
                        'app/client/dist/js/main.bundle.js'
                    );
                } else {
                    Requirements::css(
                        'app/client/dist/css/security.bundle.css'
                    );
                }
            }
        }
    }
}
