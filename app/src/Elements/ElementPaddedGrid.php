<?php

namespace DDL\Elements;
use \Page;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TreeDropdownField;
use DNADesign\Elemental\Models\ElementContent;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ElementPaddedGrid extends ElementContent
{
    private static $table_name = "ElementPaddedGrid";

    private static $singular_name = 'PaddedGrid';

    private static $description = 'Padded grid with content and call to action';

    private static $db = [
        "PaddedGrid" => "HTMLText"
    ];
    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function(FieldList $fields) {
            $fields
                ->insertAfter('HTML',
                    HTMLEditorField::create("PaddedGrid",
                    "Padded Grid")
            );
        });

        return parent::getCMSFields();
    }

    public function getType()
    {
        return "Padded Grid with Call to Action";
    }
}