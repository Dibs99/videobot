<?php

namespace DDL\Elements;
use \Page;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Control\Controller;
use SilverStripe\Forms\TreeDropdownField;
use DNADesign\Elemental\Models\ElementContent;

class ElementContentImage extends ElementContent
{
    private static $table_name = "ElementContentImage";

    private static $singular_name = 'Content Image';

    private static $description = 'Image, Content and Call to Action';

    private static $db = [
        "Subtitle" => "Varchar",
    ];

    private static $has_one = [
        "ContentImage" => Image::class
    ];

    private static $owns = ["ContentImage"];

    public function getType()
    {
        return "Content Image Block";
    }

    public function AspectRatio()
    {
        $aspectRatio = $this->ContentImage()->getHeight()/ $this->ContentImage()->getWidth();
        if ($aspectRatio < 0.25) {
            return 'narrowscreen';
        }
        if ($aspectRatio < 0.43) {
            return 'widescreen';
        } elseif ($aspectRatio > 0.57 && $aspectRatio < 0.75) {
            return 'average';
        } else {
            return 'box';
        }
    }

    public function SectionedPage()
    {   
        $controller = Controller::curr();
        if (isset($controller->SectionedPage)) {
            return Controller::curr()->SectionedPage;
        }

       return false;
    }
}