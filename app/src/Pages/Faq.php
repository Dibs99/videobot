<?php

namespace DDL\Pages;

use \Page;
use DDL\Models\Faqs;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use DDL\Utility\GoogleMap;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class Faq extends Page
{
    private static $table_name = "FaqPage";

    private static $singular_name = "Faq Page";

    private static $has_many = [
           'Faqs' => Faqs::class 
    ];

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            $config = GridFieldConfig_RecordEditor::create();
            $gridfield = GridField::create('Faqs', 'Faqs', $this->Faqs());
            $gridfield->setConfig($config);
            $fields->addFieldToTab(
                'Root.Faqs',
                $gridfield
            );
        });

        return parent::getCMSFields();
    }

    public function getOverlayMenuBar()
    {
        return true;
    }

    public function Test()
    {
        return GoogleMap::create();
    }
}