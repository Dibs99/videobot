<?php

namespace DDL\Pages;

use \PageController;
use DDL\Forms\EngageBayForm;
use SilverStripe\View\Parsers\ShortcodeParser;
use DDL\SSReactForms\Extensions\SchemaEnabledControllerExtension;

class VideoSectionedPageController extends PageController
{

    private static $extensions = [
        SchemaEnabledControllerExtension::class
    ];

    public function getEngageBayForm()
    {
        return EngageBayForm::create($this,'EngageBayForm');
    }
}