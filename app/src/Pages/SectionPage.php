<?php

namespace DDL\Pages;

use \Page;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use DNADesign\Elemental\Extensions\ElementalPageExtension;


class SectionedPage extends Page
{
    
    private static $table_name = "SectionedPage";

    private static $extensions = [ElementalPageExtension::class];

    public $SectionedPage = true;

    public function getOverlayMenuBar()
    {
        return true;
    }
}