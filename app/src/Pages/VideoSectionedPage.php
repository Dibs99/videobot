<?php

namespace DDL\Pages;

use \Page;
use SilverStripe\Dev\Debug;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use DNADesign\Elemental\Extensions\ElementalPageExtension;


class VideoSectionedPage extends Page
{
    private static $table_name = "VideoSectionedPage";


    private static $has_one = [
        'VideoBorder' => Image::class
    ];

    private static $owns = ['VideoBorder'];
    
    private static $db = [
        'VidURL' => 'HTMLText',
        'Subtit' => 'Varchar',
        'EmailFormTitle' => 'Varchar'
    ];

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function(FieldList $fields) {
            
            $fields->addFieldToTab(
                'Root.Main',
                UploadField::create('VideoBorder','Video border'),
                'MenuTitle'
            );
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create('VidURL','Video url')
                    ->setDescription("On the video you'd like to supply, click share, then embed, then copy the link here"),
                'VideoBorder'
            );
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create('Subtit','Subtitle'),
                'URLSegment'
            );
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create('EmailFormTitle'),
                'Content'
            );
        });
        
        return parent::getCMSFields();
    }
    private static $extensions = [ElementalPageExtension::class];


    public function getOverlayMenuBar()
    {
        return true;
    }

}