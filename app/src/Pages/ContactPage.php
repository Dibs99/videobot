<?php

namespace DDL\Pages;

use \Page;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;

class ContactPage extends Page
{
    private static $db = [
        'Email' => 'Varchar',
        'PhoneNumber' => 'Varchar',
        'PoBox' => 'Varchar',
    ];

    private static $singular_name = "ContactPage";
    private static $table_name = "ContactPage";

    public function getCMSFields()
    {

        $this->beforeUpdateCMSFields(function(FieldList $fields) {
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create('PoBox'),
                'Content'
            );
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create('PhoneNumber'),
                'PoBox'
            );
            $fields->addFieldToTab(
                'Root.Main',
                TextField::create('Email'),
                'PhoneNumber'
            );
            
            

        });

        return parent::getCMSFields();
    }
}