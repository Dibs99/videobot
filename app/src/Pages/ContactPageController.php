<?php

namespace DDL\Pages;

use \PageController;
use DDL\Forms\ContactForm;
use SilverStripe\View\Parsers\ShortcodeParser;
use DDL\SSReactForms\Extensions\SchemaEnabledControllerExtension;

class ContactPageController extends PageController
{

    private static $extensions = [
        SchemaEnabledControllerExtension::class
    ];

    public function getContactForm()
    {
        return ContactForm::create($this,'ContactForm');
    }

}