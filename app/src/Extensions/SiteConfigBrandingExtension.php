<?php

namespace DDL\Extensions;

use \Page;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class SiteConfigBrandingExtension extends DataExtension
{
    private static $db = [
        'CommunityFooter' => 'HTMLText',
        'FacebookUrl' => 'Varchar',
        'InstagramUrl' => 'Varchar',
        'LinkedInUrl' => 'Varchar',
        'YouTubeUrl' => 'Varchar',
        'GTMTag' => 'Varchar'
    ];

    private static $has_one = [
        'BrandLogo' => Image::class,
        'BrandLogoLight' => Image::class,
    ];

    private static $owns = [
        'BrandLogo',
        'BrandLogoLight',
    ];

    public function updateCMSFields(FieldList $fields)
    {

        $fields->addFieldsToTab('Root.Branding', [
            UploadField::create('BrandLogo'),
            UploadField::create('BrandLogoLight'),
        ]);
        $fields->addFieldsToTab('Root.SocialIcons', [
            TextField::create('FacebookUrl'),
            TextField::create('InstagramUrl'),
            TextField::create('LinkedInUrl'),
            TextField::create('YouTubeUrl'),
        ]);
        $fields->addFieldToTab('Root.TagManager',
            TextField::create('GTMTag', 'Google Tag Manager ID')
        );
    }
}