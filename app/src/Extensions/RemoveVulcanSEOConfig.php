<?php

namespace DDL\Extensions;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

/**
 * Removes the "Vulcan SEO" Tab from SiteConfig
 */
class RemoveVulcanSEOConfig extends DataExtension
{
    public function updateCMSFields(FieldList $fields)
    {
        // Remove the Vulcan SEO Tab
        $fields->removeByName("VulcanSEO");
    }
}
