<?php

namespace DDL\Extensions;

use \PageController;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\CompositeField;
use SilverStripe\ORM\FieldType\DBField;
use SwiftDevLabs\CodeEditorField\Forms\CodeEditorField;

class AddSEOSiteConfig extends DataExtension
{
    private static $db = [
        'OrganisationName' => 'Varchar(255)',
        'OrganisationDescription' => 'Varchar(255)',
        'OrganisationExtraSchema' => 'Text',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab('Root.SEO', [
            TextField::create('OrganisationName'),
            TextareaField::create('OrganisationDescription'),
            ($schemaEditor = CodeEditorField::create(
                'OrganisationExtraSchema',
                'Extra Schema'
            )->setRightTitle(
                'Add extra schema data to the Organization object. Use valid JSON as an object to merge. Additional schema data here will override defaults of the same key.'
            )),
            CompositeField::create(
                LiteralField::create('SchemaPreview', $this->SchemaPreview())
            )
                ->setTitle('Schema Preview')
                ->setRightTitle(
                    "Use this field to sure Extra Schema data isn't corrupted."
                ),
        ]);

        $schemaEditor->setMode('ace/mode/json');
    }
    public function SchemaPreview()
    {
        $data = PageController::singleton()->SchemaDataComponents();

        if (count($data) > 1) {
            $formatted = [
                "@context" => 'http://schema.org',
                '@graph' => $data,
            ];
        } else {
            $formatted = array_merge(
                [
                    "@context" => 'http://schema.org',
                ],
                $data[0]
            );
        }

        $encoded = json_encode(
            $formatted,
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );

        return DBField::create_field('HTMLFragment', $encoded);
    }
}
