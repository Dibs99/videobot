<?php

namespace DDL\Utility;

use SilverStripe\View\Requirements;
use SilverStripe\View\ViewableData;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Injector\Injectable;

class GoogleMap extends ViewableData
{   
    // There are 2 modes 'interactive' or 'static'. This renders either an interactive or static map. 
    // Please enable the javascript in your index.js file if using the interactive version
    // longitude and latitude set the general geographical area that the map should be rendered in (interactive mode only)
    // zoom levels are between 0 and 20
    
    protected $zoom = 12;
    protected $mode = 'static';
    protected $address = '17A Andromeda Crescent, East Tamaki, Auckland 2013';
    protected $longitude = 174.724672;
    protected $latitude = -36.961527;
    private $google_api_key;
    protected $width = 350;
    protected $height = 200;
    protected $desktopWidth;
    protected $desktopHeight;

    public function setField($field, $value)
    {
        $this->$field = $value;
    }


    public function forTemplate()
    {   
        if ($this->mode === 'interactive') {
            Requirements::javascript("https://maps.googleapis.com/maps/api/js?key={$this->google_api_key}&callback=initMap&libraries=places&v=weekly", ["defer" => true]);
        }   
        
        return $this->renderWith('DDL\\Utility\\GoogleMap');
    }

    public function setGoogle_api_key($value)
    {
        $this->google_api_key = $value;
        return $this;
    }

    public function ConfigJson()
    {
        $data = [
            'latLng' => [
                'lat' => $this->latitude,
                'lng' => $this->longitude
            ],
            'zoom' => $this->zoom,
            'request' => [
                'query' => $this->address,
                'fields' => ['name', 'geometry']
            ],
            'google_api_key' => $this->google_api_key,
            'mode' => $this->mode
        ];


        $result = json_encode($data);

        return $result;
    }

    public function Mode()
    {
        return $this->mode;
    }

    public function Src()
    {   
        $myAddress = $this->ParseAddress();
        return "https://maps.googleapis.com/maps/api/staticmap?zoom={$this->zoom}&size={$this->width}x{$this->height}&key={$this->google_api_key}&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C{$myAddress}";
    }
    public function SrcSetMobile()
    {   
        $myAddress = $this->ParseAddress();
        return "https://maps.googleapis.com/maps/api/staticmap?zoom={$this->zoom}&scale=2&size={$this->width}x{$this->height}&key={$this->google_api_key}&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C{$myAddress} 2x";
    }
                
    public function SrcDesktop()
    {   
        $myAddress = $this->ParseAddress();

        if($this->desktopHeight && $this->desktopWidth) {
            return "https://maps.googleapis.com/maps/api/staticmap?zoom={$this->zoom}&size={$this->desktopWidth}x{$this->desktopHeight}&key={$this->google_api_key}&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C{$myAddress} 1x, https://maps.googleapis.com/maps/api/staticmap?zoom={$this->zoom}&scale=2&size={$this->desktopWidth}x{$this->desktopHeight}&key={$this->google_api_key}&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C{$myAddress} 2x";
        } else {
            return $this->Src();
        }
    }

    public function Address()
    {
        return $this->address;
    }

    public function ParseAddress()
    {
       return urlencode($this->address);
    }
    
}

