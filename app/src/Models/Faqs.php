<?php 
namespace DDL\Models;


use DDL\Pages\Faq;
use DDL\Pages\InteractivePage;
use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;



class Faqs extends DataObject
{
    private static $table_name = "Faqs";

    private static $singular_name = "Faq";

    private static $plural_name = "Faqs";

    private static $db = [
        "Question" => "Varchar",
        "Answer" => "HTMLText",
    ];

    private static $has_one = [
        "Faq" => Faq::class,
    ];


    private static $summary_fields = [
        "Question" => "Question",
        "Answer" => "Answer",
    ];


}