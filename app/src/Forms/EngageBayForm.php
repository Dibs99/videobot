<?php

namespace DDL\Forms;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use SilverStripe\Core\Convert;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Control\Director;
use SilverStripe\Core\Environment;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Control\Email\Email;
use SilverStripe\Forms\RequiredFields;
use DDL\SSReactForms\Forms\SchemaEnabledForm;


class EngageBayForm extends SchemaEnabledForm
{

    protected $apiKey;
    
    public function __construct(
        $controller = null,
        $name = self::DEFAULT_NAME,
        $fields = null,
        $actions = null,
        $validator = null
    ) {

        $fields = FieldList::create(
            TextField::create('Name', 'Add Your Name Below')
                ->addExtraClass('full-width'),
            EmailField::create('Email', 'Add Your Email Below')
                ->addExtraClass('full-width'),
        );

        $actions = FieldList::create(
            FormAction::create('doSubmit','Send')
                ->addExtraClass('primary contained')
        );

        $validator = RequiredFields::create(
            'Email',
        );

        parent::__construct($controller, $name, $fields, $actions, $validator);

        if (Environment::getEnv('SS_NOCAPTCHA_SITE_KEY') && Environment::getEnv('SS_NOCAPTCHA_SECRET_KEY')) 
        {
            $this->enableSpamProtection();
        }
    }

    public function postRequest($data)
    {   
        $client = new Client();
        $headers = ['Content-Type' => 'application/json', 'Accept' => 'application/json', 'Authorization' => $this->apiKey];
        // breaking with json_encode
        $options = [
            'headers' => $headers,
            'body' => '{"properties": [{"name": "name","value": "' . $data['Name'] . '","field_type": "TEXT","is_searchable": false,"type": "SYSTEM"},{"name": "email","value": "' . $data['Email'] . '","field_type": "TEXT","is_searchable": false,"type": "SYSTEM"}],"tags" : [{"tag": "website-sign-up"}]}'
        ];
        
        return $client->request('POST', 'https://app.engagebay.com/dev/api/panel/subscribers/subscriber', $options);

    }

    public function doSubmit($data,$form)
    {   

        try {

            $this->postRequest($data);
            $form->setMessage("We'll be in touch with more information soon!","good");
            $form->loadDataFrom([],self::MERGE_CLEAR_MISSING);
        } catch(\RuntimeException $e) {

            $form->setMessage($e->getMessage(),"bad");

        }
        
        return $form->getSchema();
        
    }
}