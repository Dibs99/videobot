<?php

namespace DDL\Forms;

use SilverStripe\Core\Convert;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Control\Director;
use SilverStripe\Core\Environment;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Control\Email\Email;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\RequiredFields;
use DDL\SSReactForms\Forms\SchemaEnabledForm;




class ContactForm extends SchemaEnabledForm
{

    private static $email_to = 'david@doodl.co.nz';
    
    public function __construct(
        $controller = null,
        $name = self::DEFAULT_NAME,
        $fields = null,
        $actions = null,
        $validator = null
    ) {

        $fields = FieldList::create(
            TextField::create('Name', 'Your Name')
                ->addExtraClass('full-width'),
            EmailField::create('Email', 'Your Email')
                ->addExtraClass('full-width'),
            TextField::create('Subject', 'Subject')
                ->addExtraClass('full-width'),
            TextareaField::create('Message','Your Message')
        );

        $actions = FieldList::create(
            FormAction::create('doSubmit','Submit')
                ->addExtraClass('primary contained')
        );

        $validator = RequiredFields::create(
            'Name',
            'Email',
        );

        parent::__construct($controller, $name, $fields, $actions, $validator);

        if (Environment::getEnv('SS_NOCAPTCHA_SITE_KEY') && Environment::getEnv('SS_NOCAPTCHA_SECRET_KEY')) 
        {
            $this->enableSpamProtection();
        }
    }

    public function doSubmit($data,$form)
    {   

            $email = Email::create();

            $email->setReplyTo($data['Email'])
              ->setHTMLTemplate('Email\\ContactForm')
              ->setData([
                  'Name' => $data['Name'],
                  'Email' => $data['Email'],
                  'Subject' => $data['Subject'],
                  'Message' => $data['Message'],
              ])
              ->setfrom('website@'.Director::baseURL())
              ->setSubject("New website enquiry from " . Convert::raw2xml($data['Name']));

            $email->setTo($this->config()->get('email_to'));
            

        if($email->send()) {
            $form->setMessage("Thanks for your message! We will be in touch as soon as possible.","good");
            $form->loadDataFrom([],self::MERGE_CLEAR_MISSING);
        } else {
            $form->setMessage("Something went wrong and your message couldn't be sent.","bad");
        }

        
        return $form->getSchema();
        
    }
}