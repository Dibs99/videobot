<?php
namespace Deployer;

require 'recipe/silverstripe.php';

// Project name
set('application', 'Video Bot');

set('shared_dirs', ['public/assets']);

set('shared_files', ['.env']);

set('writable_dirs', ['public/assets']);

set('default_stage', 'staging');

// Project repository
set('repository', 'git@bitbucket.org:doodlltd/videobot.git');

// Use Git Archive instead of clone
// From: https://github.com/deployphp/deployer/issues/729
task('deploy:update_code', function () {
    run(
        'git archive --remote {{repository}} --format tar {{branch}} | (cd {{release_path}} && tar xf -)'
    );
});

// Replace deploy:update_code with the below snippet for a versioned deployment
// set('package_name', '***package-name***');
// set('stability','stable');
// task('deploy:update_code', function () {
//     run('composer create-project {{package_name}} {{release_path}} --repository="{\"url\": \"{{repository}}\", \"type\":\"vcs\"}" --stability={{stability}} --prefer-dist --no-install --no-interaction --remove-vcs');
// });

// task('dev_cleanup', function() {
//     run('cd {{release_path}} && rm -rf app/client/src .gitattributes .gitignore webpack.config.js postcss.config.js package-lock.json deploy.php');
// });

// after('cleanup','dev_cleanup');

host('s3.doodl.co.nz')
    ->user('st3')
    ->set('keep_releases', 1)
    ->set('deploy_path', '~/container/application')
    ->set('http_user', 'www-data')
    ->set('writable_use_sudo', false)
    ->set('writable_mode', 'chmod')
    ->forwardAgent(true)
    ->stage('staging');

after('deploy:failed', 'deploy:unlock');
