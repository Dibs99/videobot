const path = require("path");
const DefinePlugin = require("webpack").DefinePlugin;

// Plugins
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const StylelintPlugin = require('stylelint-webpack-plugin');

module.exports = (env) => {
    return {
        entry: {
            main: ["./app/client/src/js/index.js"],
            security: [
                "./app/client/src/scss/security.scss",
                "./app/client/src/images/DOODL_Profile_Logo-400x400.png",
            ],
            editor: "./app/client/src/scss/editor.scss",
        },

        output: {
            path: path.resolve(__dirname, "app/client/dist/"),
            publicPath:
                env && env.production
                    ? "alpha-1-builders-website-ss4/_resources/app/client/dist/"
                    : "http://localhost:8001/dev/",
            filename: "js/[name].bundle.js",
            chunkFilename: "js/[name].bundle.js",
        },

        devServer: {
            publicPath: "/dev/",
            contentBase: path.join(__dirname, "app/client/src"),
            watchContentBase: true,
            port: 8001,
            disableHostCheck: true,
            hot: true,
            inline: true,
            stats: "errors-only",
            headers: {
                "Access-Control-Allow-Origin": "*",
            },
        },

        devtool: "source-map",

        module: {
            rules: [
                // Javascript
                {
                    test: /\.js$/,
                    //exclude: /node_modules/,
                    exclude: {
                        test: [
                            path.resolve(__dirname, "node_modules"),
                            /\bcore-js\b/,
                            /\bwebpack\b/,
                            /\bregenerator-runtime\b/,
                        ],
                        exclude: [
                            path.resolve(
                                __dirname,
                                "node_modules/ddl-silverstripe-react-forms"
                            ),
                            path.resolve(__dirname, "node_modules/@doodl"),
                        ],
                    },
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                "@babel/preset-react",
                                [
                                    "@babel/preset-env",
                                    {
                                        modules: false,
                                        useBuiltIns: "usage",
                                        corejs: 3,
                                    },
                                ],
                            ],
                            sourceType: "unambiguous",
                            plugins: [
                                "@babel/plugin-proposal-class-properties",
                            ],
                        },
                    },
                },
                // (S)CSS
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        // Use style-loader in development to enable HMR
                        ...(env && env.production
                            ? [
                                  {
                                      loader: MiniCssExtractPlugin.loader,
                                  },
                              ]
                            : [
                                  {
                                      loader: "style-loader",
                                  },
                              ]),
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: true,
                            },
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: true,
                            },
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true,
                            },
                        },
                    ],
                },
                // Files
                {
                    test: /\.(png|jpe?g|gif|svg)$/i,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "[name].[ext]",
                                outputPath: "assets",
                            },
                        },
                    ],
                },
            ],
        },

        plugins: [
            ...(env && env.production
                ? [
                      new CleanWebpackPlugin(),
                      new HtmlWebpackPlugin({
                          chunks: [],
                          templateContent: ({ htmlWebpackPlugin }) =>
                              `${htmlWebpackPlugin.tags.headTags}`,
                          filename: path.join(
                              __dirname,
                              "app/templates/Includes/Favicons.ss"
                          ),
                          inject: false,
                      }),
                      new FaviconsWebpackPlugin({
                          logo: path.join(
                              __dirname,
                              "app/client/src/images/favicon.svg"
                          ),
                          outputPath: "/assets/favicons/",
                          prefix: "assets/favicons/",
                          inject: "force",
                          favicons: {
                              background: "#ffffff",
                              theme_color: "#ffffff",
                              icons: {
                                  android: false,
                                  appleIcon: false,
                                  appleStartup: false,
                                  coast: false,
                                  favicons: true,
                                  firefox: false,
                                  windows: false,
                                  yandex: false,
                              },
                          },
                      }),
                      new FixStyleOnlyEntriesPlugin({
                          extensions: [
                              "less",
                              "scss",
                              "css",
                              "png",
                              "jpeg",
                              "jpg",
                          ],
                      }),
                  ]
                : []),
            new MiniCssExtractPlugin({
                filename: "css/[name].bundle.css",
            }),
            new DefinePlugin({
                DDL_PROVIDED_STYLES_URI: JSON.stringify(
                    path.join(__dirname, "app/client/src/scss/style.scss")
                ),
            }),
            new StylelintPlugin({
                files: "app/client/**/*.scss",
                failOnError: env && env.production,
                failOnWarning: env && env.production,
                fix: !(env && env.production),
              }),
        ],
    };
};
